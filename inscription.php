<?php
include_once 'up.inc.php';
?>

<form class="form-horizontal" role="form" method="post" action="inscription.traitement.php">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-sm-offset-5">
                <h1>INSCRIPTION</h1>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label class="control-label col-sm-2" for="nom">Nom:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="nom" placeholder="nom " name="nom" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Adresse Electronique:</label>
                <div class="col-sm-3">
                    <input type="email" class="form-control" id="email" placeholder="example@example.com " name="email" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label class="control-label col-sm-2" for="mdp">Mot de passe:</label>
                <div class="col-sm-3">
                    <input type="password" class="form-control" id="mdp" placeholder="mot de passse" name="mdp" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-offset-2 col-sm-2">
                <button type="submit" class="btn btn-default" >S'inscrire</button>
            </div>
        </div>
    </div>
</div>
</form>

