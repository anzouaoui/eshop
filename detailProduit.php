<?php
include_once 'up.inc.php';
?>

<?php
$idProduit = filter_input(INPUT_GET, 'idProduit', FILTER_SANITIZE_NUMBER_INT);
/*
 * Ajouter l'instruction permettant d'affecter l'instance de produit correspondante à l'identifiant du produit présent dans la variable $idProduit.
 */
?>
<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <?php $produit = Produits::fetch($idProduit)?>
                    <h2><?php $produit->getCategorie() ?></h2>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">

            <div class="col-md-3">
                <div class="product-f-image">
                    <!-- Image du produit -->
                    <img src="img/"  alt="">
                    <!-- Logo de la marque -->
                    <img  src="img/" alt="" />
                </div>
            </div>
            <div class="col-md-5">


                <div class="product-inner">
                    <h2 class="product-name">
                        <!-- Nom du produit -->
                    </h2>
                    <div class="product-inner-price">
                        <ins>€ <!-- Prix du produit --></ins> <del></del>
                    </div>    
                    <!-- La validation du formulaire a pour action de rappeler la page detailProduit. L'idenfiant du produit est transmis via l'url. La quantité sera transmise via la validation du formulaire -->
                    <form action="" method="post" class="cart">
                        <div class="quantity">
                            <input type="number" size="4" class="input-text qty text" title="Quantité" value="1" name="quantite" min="1" step="1">
                        </div>
                        <button class="add_to_cart_button" type="submit">Ajouter au panier</button>
                    </form>   

                    <div class="product-inner-category">
                        <p>Categorie:
                            <!-- le lien hypertexte qui permet d'afficher une catégorie de produit dans la page "consulterCategories.php". L'identifiant de la catégorie est transmis via cette url -->
                            <a href="">
                                <!-- le nom de la catégorie -->
                            </a>
                            <!-- PARTIE NON TRAITEE DANS CE TP    
                            Mise en place de tags...
                            . Tags: <a href="">awesome</a>, <a href="">best</a>, <a href="">sale</a>, <a href="">shoes</a>. -->
                        </p>
                    </div> 

                    <div role="tabpanel">
                        <ul class="product-tab" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Vos avis</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home">
                                <h2>Description du produit</h2>  
                                <p>
                                <!-- le descriptif du produit -->
                                </p>


                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="profile">
                                <h2>Votre avis</h2>
                                <div class="submit-review">
                                    <p><label for="name">Nom</label> <input name="name" type="text"></p>
                                    <p><label for="email">Email</label> <input name="email" type="email"></p>
                                    <div class="rating-chooser">
                                        <p>Votre note</p>

                                        <div class="rating-wrap-post">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                    <p><label for="review">Votre avis</label> <textarea name="review" id="" cols="30" rows="10"></textarea></p>
                                    <p><input type="submit" value="Submit"></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="single-sidebar">
                    <h2 class="sidebar-title">Rechercher un produit</h2>
                    <form action="rechercherProduit.php">
                        <input type="text" placeholder="Nom ou référence d'un produit">
                        <input type="submit" value="Recherche">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- PARTIE NON TRAITEE DANS CE TP        
    
    Produits similaires
    
                <div class="row">
                <div class="col-lg-10 col-lg-offset-2">
                    <div class="related-products-wrapper">
                <h2 class="related-products-title">Related Products</h2>
                <div class="related-products-carousel">
                    <div class="single-product">
                        <div class="product-f-image">
                            <img src="img/product-1.jpg" alt="">
                            <div class="product-hover">
                                <a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                <a href="" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                            </div>
                        </div>
    
                        <h2><a href="">Sony Smart TV - 2015</a></h2>
    
                        <div class="product-carousel-price">
                            <ins>$700.00</ins> <del>$100.00</del>
                        </div> 
                    </div>
                    <div class="single-product">
                        <div class="product-f-image">
                            <img src="img/product-2.jpg" alt="">
                            <div class="product-hover">
                                <a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                <a href="" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                            </div>
                        </div>
    
                        <h2><a href="">Apple new mac book 2015 March :P</a></h2>
                        <div class="product-carousel-price">
                            <ins>$899.00</ins> <del>$999.00</del>
                        </div> 
                    </div>
                    <div class="single-product">
                        <div class="product-f-image">
                            <img src="img/product-3.jpg" alt="">
                            <div class="product-hover">
                                <a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                <a href="" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                            </div>
                        </div>
    
                        <h2><a href="">Apple new i phone 6</a></h2>
    
                        <div class="product-carousel-price">
                            <ins>$400.00</ins> <del>$425.00</del>
                        </div>                                 
                    </div>
                    <div class="single-product">
                        <div class="product-f-image">
                            <img src="img/product-4.jpg" alt="">
                            <div class="product-hover">
                                <a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                <a href="" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                            </div>
                        </div>
    
                        <h2><a href="">Sony playstation microsoft</a></h2>
    
                        <div class="product-carousel-price">
                            <ins>$200.00</ins> <del>$225.00</del>
                        </div>                            
                    </div>
                    <div class="single-product">
                        <div class="product-f-image">
                            <img src="img/iphone6.jpg" alt="">
                            <div class="product-hover">
                                <a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                <a href="" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                            </div>
                        </div>
    
                        <h2><a href="">Sony Smart Air Condtion</a></h2>
    
                        <div class="product-carousel-price">
                            <ins>$1200.00</ins> <del>$1355.00</del>
                        </div>                                 
                    </div>
                    <div class="single-product">
                        <div class="product-f-image">
                            <img src="img/product-6.jpg" alt="">
                            <div class="product-hover">
                                <a href="" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                <a href="" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                            </div>
                        </div>
    
                        <h2><a href="">Samsung gallaxy note 4</a></h2>
    
                        <div class="product-carousel-price">
                            <ins>$400.00</ins>
                        </div>                            
                    </div>                                    
                </div>
            </div>
                </div>
            </div>-->

</div>                    

<div class="row">
    <div class="col-lg-6">
        <div class="single-sidebar">
            <h2 class="sidebar-title">Vos dernières consultations</h2>
            <ul>
                <li><a href=""></a></li>
            </ul>
        </div> 
    </div>

</div>

<!--       PARTIE NON TRAITEE DANS CE TP       

                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Products</h2>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                        <div class="thubmnail-recent">
                            <img src="img/product-thumb-1.jpg" class="recent-thumb" alt="">
                            <h2><a href="">Sony Smart TV - 2015</a></h2>
                            <div class="product-sidebar-price">
                                <ins>$700.00</ins> <del>$100.00</del>
                            </div>                             
                        </div>
                    </div>-->


<?php include_once 'down.inc.php'; ?>

