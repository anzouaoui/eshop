<?php
include_once 'up.inc.php';
?>

<div class="maincontent-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="latest-product">
                    <h2 class="section-title">Nos Produits</h2>
                    <div class="product-carousel">
                        <?php
                        $collectionProduit = Produits::fetchAll();
                        if (is_array($collectionProduit)):
                            foreach ($collectionProduit as $produit):
                                ?>
                                <div class="single-product ">
                                    <div class="product-f-image">
                                        <img src="img/<?php echo $produit->getImage() ?>" alt="">
                                        <div class="product-hover">
                                            <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Ajouter </a>
                                            <a href="detailProduit.php?idProduit = <?php echo $produit->getIdProduit() ?>" class="view-details-link">
                                                <i class="fa fa-link"></i> Détails
                                            </a>
                                        </div>
                                    </div>

                                    <h2><?php echo $produit->getLibelle() ?></h2>
                                    <div class="product-carousel-price">
                                        <ins>€<?php echo $produit->getPrix() ?></ins> <del></del>
                                    </div> 
                                </div>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End main content area -->




<?php include_once 'down.inc.php'; ?>

