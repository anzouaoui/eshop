<?php
include_once 'up.inc.php';
?>

<div class="maincontent-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="latest-product">
                    <h2 class="section-title">Nos Categories</h2>
                    <div class="product-carousel">

                        <div class="single-product ">
                            <div class="product-f-image">
                                <?php
                                $collectionCategorie = Categorie::fetchAll();
                                if (is_array($collectionCategorie)):
                                    foreach ($collectionCategorie as $categorie):
                                        ?>
                                        <img src="img/<?php echo $categorie->getImage(); ?>" alt="<?php echo $categorie->getLibelle() ?>" />
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                                <div class="product-hover">
                                    <a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Ajouter </a>
                                    <a href="#" class="view-details-link"><i class="fa fa-link"></i> Détails</a>
                                </div>
                            </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End main content area -->

