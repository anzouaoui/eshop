<?php
class Marque {
    private  $id;
    private $nom;
    private $logo;
    private static $select = "Select * from marque ";
    private static $selectById = "Select * from marque where id=:id";
    
    function getId() {
        return $this->id;
    }
    function getNom() {
        return $this->nom;
    }
    function getLogo() {
        return $this->logo;
    }
    function setId($id) {
        $this->id = $id;
    }
    function setNom($nom) {
        $this->nom = $nom;
    }
    function setLogo($logo) {
        $this->logo = $logo;
    }

    private static function arrayToMarque(Array $array){
        $c = new Marque();
        $c->id = $array["id"];
        $c->nom = $array["nom"];
        $c->logo = $array["logo"];
        
        return $c;
    }
    
    public static function fetchAll(){
        $marque = null;
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->query(Marque::$select);
        $recordSet = $pdoStatement->fetchall(PDO::FETCH_ASSOC);
        if(is_array($recordSet)){
            foreach ($recordSet as $record){
                $marque [] = Marque::arrayToMarque($record);
            }
        }
        DataBaseAccess::close();
        return $marque;
    }
    
    public static function fetch($id){
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->prepare(Marque::$selectById);
        $pdoStatement->bindParam(":id", $id);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $marque = Marque::arrayToMarque($record);
        DataBaseAccess::close();
        return $marque;
    }
}
