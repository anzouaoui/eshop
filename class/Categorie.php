<?php
class Categorie{
    private $idCategorie;
    private $libelle;
    private $image;
    private static $select = "Select * from categorie ";
    private static $selectByIdCategorie = "Select * from categorie where idCategorie=:idCategorie";
    
    public function getIdCategorie() {
        return $this->idCategorie;
    }
    public function getLibelle() {
        return $this->libelle;
    }
    public function getImage() {
        return $this->image;
    }
    public function setLibelle($libelle) {
        $this->libelle = $libelle;
    }
    public function setImage($image) {
        $this->image = $image;
    }

    private static function arrayToCategorie(Array $array){
        $ca = new Categorie();
        $ca->idCategorie = $array["idCategorie"];
        $ca->libelle = $array["libelle"];
        $ca->image = $array["image"];
        
        return $ca;
    }

    public static function fetchAll(){
        $categorie = null;
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->query(Categorie::$select);
        $recordSet = $pdoStatement->fetchall(PDO::FETCH_ASSOC);
        if(is_array($recordSet)){
            foreach ($recordSet as $record){
                $categorie [] = Categorie::arrayToCategorie($record);
            }
        }
        DataBaseAccess::close();
        return $categorie;
    }
    
    public static function fetch($idCategorie){
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->prepare(Categorie::$selectByIdCategorie);
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $categorie = Categorie::arrayToCategorie($record);
        DataBaseAccess::close();
        return $categorie;
    }
}

