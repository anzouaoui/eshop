<?php

class Utilisateur {

    private $idUtilisateur;
    private $nom;
    private $email;
    private $mdp;
    private static $insert = "Insert into Utilisateur (nom, email, mdp) values (:nom, :email, :mdp)";
    private static $update = "Update Utilisateur set nom = :nom, email = :email, mdp = :mdp";

    public function getIdUtilisateur() {
        return $this->idUtilisateur;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getMdp() {
        return $this->mdp;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setMdp($mdp) {
        $this->mdp = $mdp;
    }

    private function arrayToUtilisateur(Array $array) {
        $u = new Utilisateur;
        $u->idUtilisateur = $array["idUtilisateur"];
        $u->nom = $array["nom"];
        $u->email = $array["emai"];
        $u->mdp = $array["mdp"];

        return $u;
    }

    public function save() {
        if ($this->idUtilisateur == NULL) {
            $req = Utilisateur::$insert;
        } 
        else {
           $req = Utilisateur::$update; 
        }
        $pdo = DataBaseAccess::open();
        $nom = $pdo->quote($this->nom);
        $email = $pdo->quote($this->email);
        $mdp = $pdo->quote($this->mdp);
        $pdoStatement = $pdo->prepare($req);
        $pdoStatement->bindParam(":nom", $nom);
        $pdoStatement->bindParam(":email", $email);
        $pdoStatement->bindParam(":mdp", $mdp);
        $pdoStatement->execute();
        
        if($this->idUtilisateur == $pdo->lastInsertId()){
            $req = Utilisateur::$insert;
        }
    }

}
