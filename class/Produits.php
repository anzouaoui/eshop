<?php
class Produits{
    private $idProduit;
    private $libelle;
    private $description;
    private $prix;
    private $image;
    private $categorie;
    private $marque;
    
    /*Asseceur-Mutateur*/
    public function getIdProduit() {
        return $this->idProduit;
    }
    public function getLibelle() {
        return $this->libelle;
    }
    public function getDescription() {
        return $this->description;
    }
    public function getPrix() {
        return $this->prix;
    }
    public function getImage() {
        return $this->image;
    }
    public function getCategorie() {
        return $this->categorie;
    }
    public function getMarque() {
        return $this->marque;
    }
    public function setLibelle($libelle) {
        $this->libelle = $libelle;
    }
    public function setDescription($description) {
        $this->description = $description;
    }
    public function setPrix($prix) {
        $this->prix = $prix;
    }
    public function setImage($image) {
        $this->image = $image;
    }
    public function setCategorie($categorie) {
        $this->categorie = $categorie;
    }
    public function setMarque($marque) {
        $this->marque = $marque;
    }
    
    /*Champs privés à portée classe*/
    private static $select = "select * from produit";
    private static $selectById = "select * from produit where idProduit  =:idProduit";
    
    /*Méthode ArraytToPrduits*/
    private static function arrayToProduits(Array $array){
        $p = new Produits();
        $p->idProduit = $array["idProduit"];
        $p->libelle = $array["libelle"];
        $p->description = $array["description"];
        $p->image = $array["image"];
        $p->prix = $array["prix"];
        $p->categorie = Categorie::fetch($array["idCategorie"]);
        $p->marque = Marque::fetch($array["idMarque"]);
        return $p;
    }
    
    /*Méthode fetchAll*/
    public static function fetchAll(){
        $collectionProduit = null;
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->query(Produits::$select);
        $recordSet = $pdoStatement->fetchall(PDO::FETCH_ASSOC);
        if(is_array($recordSet)){
            foreach ($recordSet as $record){
                $collectionProduit [] = Produits::arrayToProduits($record);
            }
        }
        DataBaseAccess::close();
        return $collectionProduit;
    }
    
    /*Méthode fetch*/
    public static function fetch($idProduit){
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->prepare(Produits::$selectById);
        $pdoStatement->bindParam(":idProduit", $idProduit);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $produit = Produit::arrayToProduits($record);
        DataBaseAccess::close();
        return $produit;
    }
}